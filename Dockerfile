# Alpine Linux with OpenJDK JRE
FROM openjdk:8-jre

# copy WAR into image
COPY openidm /openidm

RUN chgrp -R 0 /openidm && \
    chmod -R g=u /openidm

# Create user and set this user
#RUN addgroup -g 1000 -S myuser && \
#    adduser -u 1000 -S myuser -G myuser 

#RUN groupadd -g 999 appuser && \
#    useradd -r -u 999 -g appuser appuser

# Change owner
#RUN chown -R appuser openidm

# Change user
USER 10001

# Expose Ports
EXPOSE 8080

# Set idm base as workdir
WORKDIR /openidm

# Start IDM
CMD ./startup.sh
