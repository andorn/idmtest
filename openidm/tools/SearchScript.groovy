/*
 * Copyright 2014-2018 ForgeRock AS. All Rights Reserved
 *
 * Use of this code requires a commercial software license with ForgeRock AS.
 * or with one of its affiliates. All use shall be exclusively subject
 * to such license between the licensee and ForgeRock AS.
 */


import static groovyx.net.http.Method.GET

import org.apache.http.client.HttpClient
import org.forgerock.openicf.connectors.groovy.OperationType
import org.forgerock.openicf.connectors.scriptedrest.ScriptedRESTConfiguration
import org.forgerock.openicf.connectors.scriptedrest.SimpleCRESTFilterVisitor
import org.forgerock.openicf.connectors.scriptedrest.VisitorParameter
import org.identityconnectors.common.logging.Log
import org.identityconnectors.framework.common.objects.Attribute
import org.identityconnectors.framework.common.objects.AttributeUtil
import org.identityconnectors.framework.common.objects.Name
import org.identityconnectors.framework.common.objects.ObjectClass
import org.identityconnectors.framework.common.objects.OperationOptions
import org.identityconnectors.framework.common.objects.SearchResult
import org.identityconnectors.framework.common.objects.Uid
import org.identityconnectors.framework.common.objects.filter.Filter

import groovyx.net.http.RESTClient

def operation = operation as OperationType
def configuration = configuration as ScriptedRESTConfiguration
def httpClient = connection as HttpClient
def connection = customizedConnection as RESTClient
def filter = filter as Filter
def log = log as Log
def objectClass = objectClass as ObjectClass
def options = options as OperationOptions
def resultHandler = handler

log.info("Entering " + operation + " Script")
println "Connection base URI: " + connection.getUri()

def query = [:]
def queryFilter = 'true'
if (filter != null) {
    queryFilter = filter.accept(SimpleCRESTFilterVisitor.INSTANCE, [
            translateName: { String name ->
                if (AttributeUtil.namesEqual(name, Uid.NAME)) {
                    return "_id"
                } else if (AttributeUtil.namesEqual(name, Name.NAME)) {
                    return "_id"
                } else if (AttributeUtil.namesEqual(name, "apiproductname")) {
                    return "apiproductname"
                } else {
                    throw new IllegalArgumentException("Unknown field name: OpenIDM Scripted REST to ApiGee");
                }
            },
            convertValue : { Attribute value ->
                    return AttributeUtil.getStringValue(value)
            }] as VisitorParameter).toString();
}

query['_queryFilter'] = queryFilter

if (null != options.pageSize) {
    query['_pageSize'] = options.pageSize
    if (null != options.pagedResultsCookie) {
        query['_pagedResultsCookie'] = options.pagedResultsCookie
    }
    if (null != options.pagedResultsOffset) {
        query['_pagedResultsOffset'] = options.pagedResultsOffset
    }
}

def searchResult = connection.request(GET) { req ->
    uri.path = 'apiproducts'
    uri.query = query

    response.success = { resp, json ->

      println "Result of search in ApiGee connector: " + json

      json.each() { value ->

      resultHandler {
        uid value
        id value
        attribute 'apiproductname', value
      }
    }
  }
}

return new SearchResult(null, -1)