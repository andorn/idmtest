/*
 * Copyright 2014-2018 ForgeRock AS. All Rights Reserved
 *
 * Use of this code requires a commercial software license with ForgeRock AS.
 * or with one of its affiliates. All use shall be exclusively subject
 * to such license between the licensee and ForgeRock AS.
 */


import static groovyx.net.http.ContentType.JSON
import static groovyx.net.http.Method.GET
import static groovyx.net.http.Method.PUT

import groovyx.net.http.RESTClient
import org.apache.http.client.HttpClient
import org.forgerock.openicf.connectors.groovy.OperationType
import org.forgerock.openicf.connectors.scriptedrest.ScriptedRESTConfiguration
import org.identityconnectors.common.logging.Log
import org.identityconnectors.framework.common.exceptions.ConnectorException
import org.identityconnectors.framework.common.objects.Attribute
import org.identityconnectors.framework.common.objects.AttributesAccessor
import org.identityconnectors.framework.common.objects.ObjectClass
import org.identityconnectors.framework.common.objects.OperationOptions
import org.identityconnectors.framework.common.objects.Uid

def operation = operation as OperationType
def updateAttributes = new AttributesAccessor(attributes as Set<Attribute>)
def configuration = configuration as ScriptedRESTConfiguration
def httpClient = connection as HttpClient
def connection = customizedConnection as RESTClient
def name = id as String
def log = log as Log
def objectClass = objectClass as ObjectClass
def options = options as OperationOptions
def uid = uid as Uid
def productDescription = "default"as String
def productQuota = "default" as String
def productDisplayName = "default" as String
java.util.List<String> envList = new java.util.ArrayList()

log.info("Entering " + operation + " Script");
println "Updating: " + name
println "Size: " + attributes.size()

attributes.each { entry ->
    println "Name: " + entry.toString()

    if ("environments".equals(entry.getName())) {
      envList.addAll(entry.getValue());
    }
}

envList.each { entry ->
  println "environment: " + entry
}

productDescription = updateAttributes.hasAttribute("description") ? updateAttributes.findString("description") : "-"
productQuota = updateAttributes.hasAttribute("quota") ? updateAttributes.findString("quota") : "1"
productDisplayName = updateAttributes.hasAttribute("displayname") ? updateAttributes.findString("displayname") : ""

def builder = new groovy.json.JsonBuilder()

builder {
    displayName productDisplayName
    description productDescription
    quota productQuota
    approvalType "manual"
    quotaInterval "1"
    quotaTimeUnit "minute"
    environments envList.collect {
      it
    }
}

println builder.toPrettyString()

switch (operation) {
    case OperationType.UPDATE:

      println "Entering update"

      return connection.request(PUT, JSON) { req ->
        uri.path = "apiproducts/${name}"
        body = builder.toString()
        headers.'If-Match' = "*"

        response.success = { resp, json ->

          println "json result:" + json
          println "Returning: " + uid.toString();

          return uid
        }
      }

      break
    case OperationType.ADD_ATTRIBUTE_VALUES:
        throw new UnsupportedOperationException(operation.name() + " operation of type:" +
                objectClass.objectClassValue + " is not supported.")
    case OperationType.REMOVE_ATTRIBUTE_VALUES:
        throw new UnsupportedOperationException(operation.name() + " operation of type:" +
                objectClass.objectClassValue + " is not supported.")
    default:
        throw new ConnectorException("UpdateScript can not handle operation:" + operation.name())
}
return uid