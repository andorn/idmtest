/*
 * Copyright 2014-2018 ForgeRock AS. All Rights Reserved
 *
 * Use of this code requires a commercial software license with ForgeRock AS.
 * or with one of its affiliates. All use shall be exclusively subject
 * to such license between the licensee and ForgeRock AS.
 */

import static org.identityconnectors.framework.common.objects.AttributeInfo.Flags.NOT_RETURNED_BY_DEFAULT
import static org.identityconnectors.framework.common.objects.AttributeInfo.Flags.NOT_UPDATEABLE
import static org.identityconnectors.framework.common.objects.AttributeInfo.Flags.NOT_READABLE

import groovyx.net.http.RESTClient
import org.apache.http.client.HttpClient
import org.forgerock.openicf.connectors.groovy.OperationType
import org.forgerock.openicf.connectors.scriptedrest.ScriptedRESTConfiguration
import org.identityconnectors.common.logging.Log
import org.identityconnectors.framework.common.objects.AttributeInfoBuilder
import org.identityconnectors.framework.common.objects.ObjectClass

def operation = operation as OperationType
def configuration = configuration as ScriptedRESTConfiguration
def httpClient = connection as HttpClient
def connection = customizedConnection as RESTClient
def log = log as Log

log.info("Entering " + operation + " Script");

// Declare the __ACCOUNT__ attributes
// _id
def idAIB = new AttributeInfoBuilder("__NAME__", String.class);
idAIB.setRequired(true);
idAIB.setCreateable(true);
idAIB.setMultiValued(false);
idAIB.setUpdateable(false);

// userName
def userNameAIB = new AttributeInfoBuilder("userName", String.class);
userNameAIB.setCreateable(false);
userNameAIB.setMultiValued(false);
userNameAIB.setUpdateable(false);

// displayName
def displayNameAIB = new AttributeInfoBuilder("displayName", String.class);
displayNameAIB.setRequired(true);
displayNameAIB.setMultiValued(false);

// group displayName
def grpDisplayNameAIB = new AttributeInfoBuilder("displayName", String.class);
grpDisplayNameAIB.setMultiValued(false);
grpDisplayNameAIB.setCreateable(false);
grpDisplayNameAIB.setUpdateable(false);

// familyName
def familyNameAIB = new AttributeInfoBuilder("familyName", String.class);
familyNameAIB.setRequired(true);
familyNameAIB.setMultiValued(false);

// givenName
def givenNameAIB = new AttributeInfoBuilder("givenName", String.class);
givenNameAIB.setMultiValued(false);

// emailAddress
def emailAddressAIB = new AttributeInfoBuilder("emailAddress", String.class);
emailAddressAIB.setMultiValued(false);

//created
def createdAIB = new AttributeInfoBuilder("created", String.class);
createdAIB.setCreateable(false);
createdAIB.setMultiValued(false);
createdAIB.setUpdateable(false);

//lastModified
def lastModifiedAIB = new AttributeInfoBuilder("lastModified", String.class);
lastModifiedAIB.setCreateable(false);
lastModifiedAIB.setMultiValued(false);
lastModifiedAIB.setUpdateable(false);

// api product name
def apiProductNameAIB = new AttributeInfoBuilder("apiproductname", String.class);
apiProductNameAIB.setCreateable(true);
apiProductNameAIB.setMultiValued(false);
apiProductNameAIB.setUpdateable(true);
apiProductNameAIB.setRequired(true);

// api product description
def apiProductDescAIB = new AttributeInfoBuilder("description", String.class);
apiProductNameAIB.setCreateable(true);
apiProductNameAIB.setMultiValued(false);
apiProductNameAIB.setUpdateable(true);
apiProductNameAIB.setRequired(false);

// api quota
def apiQuotaAIB = new AttributeInfoBuilder("quota", String.class);
apiQuotaAIB.setCreateable(true);
apiQuotaAIB.setMultiValued(false);
apiQuotaAIB.setUpdateable(true);
apiQuotaAIB.setRequired(false);

// api display name
def apiDisplayNameAIB = new AttributeInfoBuilder("displayname", String.class);
apiDisplayNameAIB.setCreateable(true);
apiDisplayNameAIB.setMultiValued(false);
apiDisplayNameAIB.setUpdateable(true);
apiDisplayNameAIB.setRequired(false);


// api environments name
def apiEnvirnomentsAIB = new AttributeInfoBuilder("environments", String.class);
apiDisplayNameAIB.setCreateable(true);
apiDisplayNameAIB.setMultiValued(true);
apiDisplayNameAIB.setUpdateable(true);
apiDisplayNameAIB.setRequired(false);

return builder.schema({
    objectClass {
        type ObjectClass.ACCOUNT_NAME
        attribute apiProductNameAIB.build()
        attribute apiProductDescAIB.build()
        attribute apiQuotaAIB.build()
        attribute apiDisplayNameAIB.build()
        attribute apiEnvirnomentsAIB.build()
    }
})

log.info("Schema script done");