/*
 * Copyright 2014-2018 ForgeRock AS. All Rights Reserved
 *
 * Use of this code requires a commercial software license with ForgeRock AS.
 * or with one of its affiliates. All use shall be exclusively subject
 * to such license between the licensee and ForgeRock AS.
 */

import static groovyx.net.http.ContentType.JSON

import groovy.json.JsonBuilder
import groovyx.net.http.RESTClient
import org.apache.http.client.HttpClient
import org.forgerock.openicf.connectors.groovy.OperationType
import org.forgerock.openicf.connectors.scriptedrest.ScriptedRESTConfiguration
import org.identityconnectors.common.logging.Log
import org.identityconnectors.framework.common.objects.Attribute
import org.identityconnectors.framework.common.objects.AttributesAccessor
import org.identityconnectors.framework.common.objects.ObjectClass
import org.identityconnectors.framework.common.objects.OperationOptions

def operation = operation as OperationType
def createAttributes = new AttributesAccessor(attributes as Set<Attribute>)
def configuration = configuration as ScriptedRESTConfiguration
def httpClient = connection as HttpClient
def connection = customizedConnection as RESTClient
def productName = id as String
def log = log as Log
def objectClass = objectClass as ObjectClass
def options = options as OperationOptions
def productDescription = "default "as String
def productQuota = "default "as String
def productDisplayName = "default "as String
java.util.List<String> envList = new java.util.ArrayList()

log.info("Entering " + operation + " Script");
log.info("Creating: " + productName);
println "Entering " + operation + " Script";

attributes.each { entry ->
  println "value: " + entry.toString()

  if ("environments".equals(entry.getName())) {
    envList.addAll(entry.getValue());
  }
}

envList.each { entry ->
  println "environment: " + entry
}

println "List finished."

productDescription = createAttributes.hasAttribute("description") ? createAttributes.findString("description") : ""
productQuota = createAttributes.hasAttribute("quota") ? createAttributes.findString("quota") : "1"
productDisplayName = createAttributes.hasAttribute("displayname") ? createAttributes.findString("displayname") : ""

def jsonInput = new groovy.json.JsonBuilder()
jsonInput {
    name productName
    displayName productDisplayName
    description productDescription
    quota productQuota
    approvalType "manual"
    quotaInterval "1"
    quotaTimeUnit "minute"
    environments envList.collect {
      it
    }
}

println "JSON:" + jsonInput.toPrettyString()

connection.post(
        path: 'apiproducts',
        headers: ['If-None-Match': '*'],
        contentType: JSON,
        requestContentType: JSON,
        body: jsonInput.toString());

return productName